const gulp = require('gulp')
const scss = require('gulp-sass')
const browserSync = require('browser-sync').create()
const concat = require('gulp-concat')
const uglify = require('gulp-uglify-es').default
const cleanCSS = require('gulp-clean-css')
const rename = require('gulp-rename')
const del = require('del')
const imagemin = require('gulp-imagemin')
const cache = require('gulp-cache')
const autoprefixer = require('gulp-autoprefixer')
const ftp = require('vinyl-ftp')
const notify = require('gulp-notify')
const rsync = require('gulp-rsync')

gulp.task('browser-sync', function () {
  browserSync.init({
    server: {baseDir: 'app'},
    notify: false
  })
})

function bsReload(done) {
  browserSync.reload()
  done()
}

gulp.task('scss', function () {
  return gulp.src('app/scss/**/*.scss')
    .pipe(scss({outputStyle: 'expanded'}).on('error', notify.onError()))
    .pipe(rename({suffix: '.min', prefix: ''}))
    .pipe(autoprefixer({overrideBrowserslist: ['last 10 versions']}))
    .pipe(cleanCSS()) // Опционально, закомментировать при отладке
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.stream())
})

gulp.task('js', function () {
  return gulp.src([
    'app/libs/jquery/jquery.min.js',
    'app/libs/slick/slick.min.js',
    'app/js/common.js' // Всегда в конце
  ])
    .pipe(concat('scripts.min.js'))
    .pipe(uglify()) // Минимизировать весь js (на выбор)
    .pipe(gulp.dest('app/js'))
    .pipe(browserSync.reload({stream: true}))
})

gulp.task('imagemin', function () {
  return gulp.src('app/img/**/*')
    .pipe(cache(imagemin())) // Cache Images
    .pipe(gulp.dest('public/img'))
})

gulp.task('removePublic', function () {
  return del(['public'], {force: true})
})
gulp.task('clearcache', function () {
  return cache.clearAll()
})
gulp.task('buildFiles', function () {
  return gulp.src(['app/*.html', 'app/.htaccess']).pipe(gulp.dest('public'))
})
gulp.task('buildCss', function () {
  return gulp.src(['app/css/main.min.css']).pipe(gulp.dest('public/css'))
})
gulp.task('buildJs', function () {
  return gulp.src(['app/js/scripts.min.js']).pipe(gulp.dest('public/js'))
})
gulp.task('buildFonts', function () {
  return gulp.src(['app/fonts/**/*']).pipe(gulp.dest('public/fonts'))
})

gulp.task('build', gulp.series('removePublic', 'imagemin', 'scss', 'js', 'buildFiles', 'buildCss', 'buildJs', 'buildFonts'))

gulp.task('deploy', function () {
  const conn = ftp.create({
    host: 'hostname.com',
    user: 'username',
    password: 'userpassword',
    parallel: 10
  })

  const globs = ['public/**', 'public/.htaccess']
  return gulp.src(globs, {buffer: false})
    .pipe(conn.dest('/path/to/folder/on/server'))
})

gulp.task('rsync', function () {
  return gulp.src('app/')
    .pipe(rsync({
      root: 'public/',
      hostname: 'username@yousite.com',
      destination: 'yousite/public_html/',
      exclude: ['**/Thumbs.db', '**/*.DS_Store'],
      recursive: true,
      archive: true,
      silent: false,
      compress: true
    }))
})

gulp.task('code', function () {
  return gulp.src('app/**/*.html')
    .pipe(browserSync.reload({stream: true}))
})

gulp.task('watch', function () {
  gulp.watch('app/scss/**/*.scss', gulp.parallel('scss'))
  gulp.watch(['libs/**/*.js', 'app/js/common.js'], gulp.parallel('js'))
  gulp.watch('app/*.html', gulp.parallel('code'))
})

gulp.task('default', gulp.parallel('scss', 'js', 'browser-sync', 'watch'))
