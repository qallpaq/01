$(document).ready(() => {
  function paintHeader() {
    const header = $('.header')

    window.pageYOffset > 0
      ? header.addClass('active')
      : header.removeClass('active')
  }

  (function ($) {
      const setMenu = DOMElement => {
        $(DOMElement).on('click', () => {
          $('.mobile-navigation').toggleClass('active')
        })
      }
      setMenu('.burger')
      setMenu('.mobile-navigation__item')
    }
  )(jQuery)

  function goToDown(scroll) {
    const headerHeight = 100
    const extraPixels = 100
    const recursion = setTimeout(goToDown, 10, scroll + extraPixels)

    if (scroll >= window.innerHeight) {
      clearTimeout(recursion)
      return window.scrollTo(0, window.innerHeight - headerHeight)
    }

    window.scrollTo(0, scroll)
    return recursion
  }

  (function ($) {
    $(function () {
      $('ul.tabs__header').on('click', 'li:not(.active)', function () {
        $(this)
          .addClass('active')
          .siblings()
          .removeClass('active')
          .closest('.tabs')
          .find('.tabs__content-item')
          .removeClass('active')
          .eq($(this).index())
          .addClass('active')
      })
    })
  })(jQuery)

  $('.homepage__arrow').on('click', () => goToDown(window.scrollY))

  paintHeader()

  $(document).on('scroll', paintHeader)

  $('.slick').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    swipe: false,
    responsive: [{breakpoint: 1024, settings: {swipe: true}}]
  })
})
